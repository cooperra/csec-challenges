#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FLAG_LEN 64
#define BUF_LEN 128

// Hello World

void printflag ()
{
    char flagbuf[FLAG_LEN];
    memset (flagbuf, 0, FLAG_LEN);

    FILE * flagfile = fopen("flag.txt", "r");
    fread (flagbuf, 1, FLAG_LEN - 1, flagfile);
    fclose (flagfile);

    printf ("%s\n", flagbuf);
}

int main ()
{
    char inbuf[BUF_LEN];
    memset (inbuf, 0, BUF_LEN);

    printf ("Hello: \n");
    fgets (inbuf, BUF_LEN, stdin);
    printf (inbuf);
}