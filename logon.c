#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define ULEN 256
#define PLEN 64

int authenticate (char * fuser, char * fpass)
{
    // this struct forces the buffers to be in a certain order on the stack
    struct {
    int ucounter;
    char in; // char gor getting input from user
    char username[ULEN]; // buffer to store username
    char password[PLEN]; // buffer to store password input
    } locals;

    //initialize the variables
    memset (locals.username, 0, ULEN);
    memset (locals.password, 0, PLEN);
    locals.ucounter = 0;
    locals.in = '\0';


    printf ("Please enter your username:\n");
    fflush(stdout); // allows another program I have to pipe stdout over a network

    //read in characters (%x to reveal memory)
    locals.in = getchar();
    while (32 <= (int) locals.in && (int) locals.in < 127 && locals.ucounter < ULEN-1) {
        locals.username[locals.ucounter] = locals.in;
        locals.ucounter++;
        locals.in = getchar();
    }
    printf ("Greetings ");
    printf (locals.username); // allows them to reveal the canary
    fflush(stdout);

    printf ("\nPlease enter your password:\n");
    fflush(stdout);

    locals.ucounter = 0;
    locals.in = getchar();
    while (locals.in != '\n' && locals.ucounter < ULEN-1) { //this bad comparison is on purpose.  This is how to exploit the program
        locals.password[locals.ucounter] = locals.in;
        locals.ucounter++;
        locals.in = getchar();
    }
    printf (locals.username); // I think this is debug and isn't needed for the program
    fflush(stdout);
    if (strncmp (fuser, locals.username, strlen(fuser)) == 0 && strncmp (fpass, locals.password, strlen(fpass)) == 0) {
        return 1;
    }
    return 0;
}

int main()
{
    /* Most of main is just to read a file to get username and password values to use */


    // don't care about variable order on stack here
    char user[ULEN];
    char pass[PLEN];
    FILE * userfile;
    FILE * passfile;
    size_t rsize;
    int compres;

    memset (user, 0, ULEN);
    memset (pass, 0, PLEN);

    userfile = fopen("user.txt", "r");
    rsize = fread (user, 1, ULEN-1, userfile);
    fclose (userfile);

    passfile = fopen("pass.txt", "r");
    rsize = fread (pass, 1, PLEN-1, passfile);
    fclose (passfile);

    // get rid of the newlines in the file
    user[strlen(user) - 1] = '\0';
    pass[strlen(pass) - 1] = '\0';

    // call the vulnerable function and return the result
    compres = authenticate(user, pass);
    if (compres > 0) {
        execl ("/bin/bash", NULL, NULL);
    }
}